unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFm_principal = class(TForm)
    List: TListBox;
    But_inserir: TButton;
    But_remover: TButton;
    But_att: TButton;
    But_save: TButton;
    Edit_mat: TEdit;
    Edit_nota: TEdit;
    Edit_recup: TEdit;
    Btu_carregar: TButton;
    procedure Edit_matClick(Sender: TObject);
    procedure Edit_notaClick(Sender: TObject);
    procedure Edit_recupClick(Sender: TObject);
    procedure But_inserirClick(Sender: TObject);
    procedure But_removerClick(Sender: TObject);
    procedure But_attClick(Sender: TObject);
    procedure But_saveClick(Sender: TObject);
  private

  public

  end;

var
  Fm_principal: TFm_principal;

implementation
  type
    TProva = class(TObject)
    mat,nota,recup : string;
  end;

  var Prova : TProva;
      Arq : TextFile;
{$R *.dfm}


procedure TFm_principal.Edit_matClick(Sender: TObject);
begin
  Edit_mat.Clear;
  Edit_mat.Font.Color := 000000;
end;

procedure TFm_principal.Edit_notaClick(Sender: TObject);
begin
  Edit_nota.Clear;
  Edit_nota.Font.Color := 000000;
end;

procedure TFm_principal.Edit_recupClick(Sender: TObject);
begin
  Edit_recup.Clear;
  Edit_recup.Font.Color := 000000;
end;
  //


procedure TFm_principal.But_inserirClick(Sender: TObject);
begin
  Prova := TProva.Create;

  If (Edit_mat.text <> '') and (Edit_nota.text <> '') and (Edit_recup.text <> '') and
     (Edit_mat.text <> 'Materia da Prova') and (Edit_nota.text <> 'Nota da Prova') and (Edit_recup.text <> 'Nota da Recuperacao')

   then
  begin
    Prova.mat := Edit_mat.text;
    Prova.nota := Edit_nota.text;
    Prova.recup := Edit_recup.text;

    Fm_principal.List.Items[1] := Prova.mat;
    Fm_principal.List.Items[3] := Prova.nota;
    Fm_principal.List.Items[5] := Prova.recup;
  end
  Else
  Begin
    ShowMessage ('ERRO! Ha um campo nao preenchido ');
  end;                   
end;

procedure TFm_principal.But_removerClick(Sender: TObject);
begin
    If (List.ItemIndex = 1) Then
    Begin
      Fm_principal.List.Items[List.ItemIndex] := ' ';
    end;

    If (List.ItemIndex = 3) Then
    Begin
      Fm_principal.List.Items[List.ItemIndex] := ' ';
    end;

    If (List.ItemIndex = 5) Then
    Begin
      Fm_principal.List.Items[List.ItemIndex] := ' ';
    end;

    If (List.ItemIndex = 7) Then
    Begin
      Fm_principal.List.Items[List.ItemIndex] := ' ';
    end;
end;



procedure TFm_principal.But_attClick(Sender: TObject);
begin

  If (List.ItemIndex = 1) and (Edit_mat.Text <> '') and (Edit_mat.Text <> 'Materia da Prova') Then
  Begin
      Prova.mat := Edit_mat.Text;
      Fm_principal.List.Items[List.ItemIndex] := Prova.mat;
  end



  Else If (List.ItemIndex = 3) and (Edit_nota.Text <> '') and (Edit_nota.Text <> 'Nota da Prova') Then
  Begin
      Prova.nota := Edit_nota.Text;
      Fm_principal.List.Items[List.ItemIndex] := Prova.nota;
  end



  Else If (List.ItemIndex = 5) and (Edit_recup.Text <> '') and (Edit_recup.Text <> 'Data de Lanšamento') Then
  Begin
      Prova.recup := Edit_recup.Text;
      Fm_principal.List.Items[List.ItemIndex] := Prova.recup;
  end


  Else If (List.ItemIndex = 7) Then
  Else
    ShowMessage ('Selecione um item');
  end;
procedure TFm_principal.But_saveClick(Sender: TObject);
begin
  AssignFile(Arq, Edit_mat.text + '.txt' );
  Rewrite(Arq);
  WriteLn(Arq, Prova.mat) ;
  WriteLn(Arq, Prova.nota);
  WriteLn(Arq, Prova.recup);
  CloseFile(Arq);

  ShowMessage('Arquivo salvo!');
end;

end.
