object Fm_principal: TFm_principal
  Left = 191
  Top = 130
  Caption = 'Programa Prova'
  ClientHeight = 243
  ClientWidth = 509
  Color = clGradientInactiveCaption
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'DejaVu Sans Condensed'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 18
  object List: TListBox
    Left = 8
    Top = 8
    Width = 217
    Height = 225
    Color = clGradientActiveCaption
    ItemHeight = 18
    Items.Strings = (
      'Materia:'
      ''
      'Nota da Prova:'
      ''
      'Nota da Recuperacao:'
      '')
    TabOrder = 0
  end
  object But_inserir: TButton
    Left = 424
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Inserir'
    TabOrder = 1
    OnClick = But_inserirClick
  end
  object But_remover: TButton
    Left = 424
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Remover'
    TabOrder = 2
    OnClick = But_removerClick
  end
  object But_att: TButton
    Left = 424
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Atualizar'
    TabOrder = 3
    OnClick = But_attClick
  end
  object But_save: TButton
    Left = 424
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Salvar'
    TabOrder = 4
    OnClick = But_saveClick
  end
  object Edit_mat: TEdit
    Left = 240
    Top = 8
    Width = 161
    Height = 27
    Font.Charset = ANSI_CHARSET
    Font.Color = clGray
    Font.Height = -16
    Font.Name = 'DejaVu Sans Condensed'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 5
    Text = 'Materia'
    OnClick = Edit_matClick
  end
  object Edit_nota: TEdit
    Left = 240
    Top = 48
    Width = 161
    Height = 27
    Font.Charset = ANSI_CHARSET
    Font.Color = clGray
    Font.Height = -16
    Font.Name = 'DejaVu Sans Condensed'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 6
    Text = 'Nota da Prova'
    OnClick = Edit_notaClick
  end
  object Edit_recup: TEdit
    Left = 240
    Top = 88
    Width = 161
    Height = 27
    Font.Charset = ANSI_CHARSET
    Font.Color = clGray
    Font.Height = -16
    Font.Name = 'DejaVu Sans Condensed'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 7
    Text = 'Nota da Recuperacao'
    OnClick = Edit_recupClick
  end
  object Btu_carregar: TButton
    Left = 426
    Top = 159
    Width = 75
    Height = 25
    Caption = 'Carregar'
    TabOrder = 8
  end
end
